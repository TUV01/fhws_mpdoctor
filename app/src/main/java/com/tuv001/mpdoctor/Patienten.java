package com.tuv001.mpdoctor;
/**
 * Created by JTJ
 */
//GUI (JTJ)
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.SearchView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
//DB (Magommed)
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public class Patienten extends AppCompatActivity {


    private ArrayList<Patient> patientsList;
    private ListView lv;
    private SearchView sv;


    // @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patienten);

        lv=(ListView) findViewById(R.id.listView);
        sv=(SearchView) findViewById(R.id.searchView);

        getPatientData(); // Receive the data from the DB

        //ADAPTER
        final PatientAdapter adapter=new PatientAdapter(this, patientsList);
        lv.setAdapter(adapter);

        //Search function
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String arg0) {
                // TODO Auto-generated method stub
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                // TODO Auto-generated method stub
                adapter.getFilter().filter(query);
                return false;
            }
        });
    }

    //Method that translates status into Spanish or English
    private String gettranslation(String status)
    {
        Map<String,String> english=new HashMap<String, String>();
        Map<String,String> spanish=new HashMap<String, String>();
        english.put("Ja","Yes"); spanish.put("Ja","Si");
        english.put("Nein","No"); spanish.put("Nein","No");

        //Get the current language
        String language= Locale.getDefault().getLanguage();

        if(language.equals("es"))
        {
            status=spanish.get(status);
        }
        else if(language.equals("en"))
        {
            status=english.get(status);
        }
        return status;
    }

    //Method created by Magommed + GUI(JTJ)
    private void getPatientData() {

        //GUI(JTJ) from *
        //Adding data to ArrayList
        patientsList = new ArrayList<Patient>();
        //Recovering from SharedPreferences
        SharedPreferences prefs = getSharedPreferences("Mypreferences", Context.MODE_PRIVATE);
        final String UsernameShared=prefs.getString("username","forexampleusername");
        //to *

        boolean isSuccess;
        Connection con;
        String z = "";
        String krankheit = null; // Ermittelte Krankheit hinzufügen

        try {
            ConnectionDB cDB = new ConnectionDB();
            con = cDB.connectDB();   // Connect to database
            if (con == null) {
                z = "Check Your Internet Access!";
            } else {
                String query = "select lastName, firstName, status from patienten";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    z = "Patient data received";
                    patientsList.add(new Patient(rs.getString("lastName"),rs.getString("firstName"),gettranslation(rs.getString("status"))));
                }

                stmt.close();
                con.close();

            }

        } catch (Exception ex) {

            z = ex.getMessage();
        }
    }

}
