package com.tuv001.mpdoctor;
/**
 * Created by JTJ
 */

//GUI (JTJ)
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
//DB (Magommed)
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public class MedicalHistory extends AppCompatActivity {

    private ArrayList<History> historyList;
    private ListView lv;
    private Button buttonMHsend;//Send medical history request
    private EditText firstname;
    private EditText lastname;
    private String enteredfirstname;//Patient's firstname
    private String enteredlastname;//Patient's lastname

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_history);

        lv=(ListView) findViewById(R.id.listView);
        buttonMHsend=(Button)findViewById(R.id.buttonSendMedicalHistory);
        firstname=(EditText)findViewById(R.id.etpatientfirstname);
        lastname=(EditText)findViewById(R.id.etpatientlastname);

        //ButtonSEndMedicalHistory
        buttonMHsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                enteredfirstname = firstname.getText().toString();
                enteredlastname = lastname.getText().toString();

                if (enteredlastname.equals("") || enteredfirstname.equals("")) {
                    Toast.makeText(MedicalHistory.this, getString(R.string.messagefillhistory), Toast.LENGTH_LONG).show();
                    //Error msg if last name or first name is equals " "
                    return;
                }


                //Created by Magommed from *
                else {
                    boolean isSuccess;
                    Connection con;
                    String z = "";

                    try {
                        ConnectionDB cDB = new ConnectionDB();
                        con = cDB.connectDB(); // Connect to database
                        if (con == null) {
                            z = "Check Your Internet Access!";
                        } else {
                            String query = "select * from history where Lastname = '" + enteredlastname.toString() + "' and Firstname = '" + enteredfirstname.toString() + "'  ";
                            Statement stmt = con.createStatement();
                            ResultSet rs = stmt.executeQuery(query);
                            if (rs.next()) {
                                z = "successful";
                                isSuccess = true;
                                getMedicalHistory(); //Medical History
                                con.close();
                            } else {
                                z = "Invalid Credentials!";
                                isSuccess = false;
                            }
                        }

                    } catch (Exception ex) {

                        z = ex.getMessage();
                    }

                }// to *


            }
        });


    }

    //Method getMedicalHistory created by JTJ
    private void getMedicalHistory()
    {
        gethistoryData(enteredfirstname,enteredlastname);// Receive the data from the DB
        //ADAPTER
        final HistoryAdapter adapter=new HistoryAdapter(this, historyList); //
        lv.setAdapter(adapter);
    }


    //Method gettranslation created by JTJ
    //Method that translates disease into Spanish or English
    private String gettranslation(String disease)
    {
        Map<String,String> english=new HashMap<String, String>();
        Map<String,String> spanish=new HashMap<String, String>();
        english.put("Lebensmittelvergiftung","Food poisoning"); spanish.put("Lebensmittelvergiftung","Intoxicación Alimentaria");
        english.put("Grippe","Flu");spanish.put("Grippe","Gripe");
        english.put("Erkältung","Cold");spanish.put("Erkältung","Resfriado");
        english.put("Migräne","Migraine");spanish.put("Migräne","Migraña");
        english.put("Gicht","Gout");spanish.put("Gicht","Gota");
        english.put("Blinddarmentzündung","Appendicitis");spanish.put("Blinddarmentzündung","Apendicitis");
        english.put("Mandelentzündung","Amygdalitis");spanish.put("Mandelentzündung","Amigdalitis");
        english.put("Heuschnupfen","Hay fever");spanish.put("Heuschnupfen","Fiebre del heno");
        english.put("Lungenentzündung","Pneumonia");spanish.put("Lungenentzündung","Neumonía");
        english.put("Mittelohrentzündung","Otitis");spanish.put("Mittelohrentzündung","Otitis");

        //Get the current language
        String language= Locale.getDefault().getLanguage();

        if(language.equals("es"))
        {
            disease=spanish.get(disease);
        }
        else if(language.equals("en"))
        {
            disease=english.get(disease);
        }
        return disease;
    }


    private void gethistoryData(String firstname,String lastname)
    {
        //Adding data to ArrayList
        historyList = new ArrayList<History>();

        boolean isSuccess;
        Connection con;
        String z = "";

        try {
            ConnectionDB cDB = new ConnectionDB();
            con = cDB.connectDB();   // Connect to database
            if (con == null) {
                z = "Check Your Internet Access!";
            } else {
                //SELECT * FROM `History` WHERE `Lastname`='Mourinho' AND `Firstname`='José'
                String query = "select date, disease, medicament from history where Lastname = '"
                        + enteredlastname.toString() + "' and Firstname = '" + enteredfirstname.toString() + "'  ";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    z = "Patient data received";
                    historyList.add(new History(rs.getString("date"), gettranslation(rs.getString("disease")), rs.getString("medicament")));
                }
                stmt.close();
                con.close();

            }

        } catch (Exception ex) {

            z = ex.getMessage();
        }

    }

}
