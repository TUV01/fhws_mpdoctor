package com.tuv001.mpdoctor;

/**
 * Created by JTJ-PC on 02.05.2017.
 */

public class Patient
{
    String vorname;
    String nachname;
    String status;

    public Patient(String nachname, String vorname, String status) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.status = status;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
