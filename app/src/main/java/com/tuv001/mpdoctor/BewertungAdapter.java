package com.tuv001.mpdoctor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by JTJ-PC on 01.05.2017.
 */

public class BewertungAdapter extends BaseAdapter
{
    private Context activity;
    private ArrayList<Bewertung> data;
    private static LayoutInflater inflater = null;
    private View vi;
    private BewertungAdapter.ViewHolder viewHolder;

    public BewertungAdapter(Context context, ArrayList<Bewertung> bewertungs) {
        this.activity = context;
        this.data = bewertungs;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        vi = view;
        //Populate the Listview
        final int pos = position;
        Bewertung bewertungs = data.get(pos);
        if(view == null) {
            vi = inflater.inflate(R.layout.listview_rating, null);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) vi.findViewById(R.id.medicaments);
            viewHolder.punkte=(TextView) vi.findViewById(R.id.ratingscale);
            vi.setTag(viewHolder);

        }else
            viewHolder = (BewertungAdapter.ViewHolder) view.getTag();
        viewHolder.name.setText(bewertungs.getMedikament());
        viewHolder.punkte.setText(bewertungs.getWert());
        return vi;
    }
    public ArrayList<Bewertung> getAllData(){
        return data;
    }


    public class ViewHolder{
        TextView name;
        TextView punkte;

    }
}
