package com.tuv001.mpdoctor;
/**
 * Created by JTJ-PC on 01.05.2017.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ArztMain extends AppCompatActivity {

    private Button patienten;
    private Button medicamentsranking;
    private Button historypatienten;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arzt_main);


        //Button medicaments ranking
        medicamentsranking=(Button)findViewById(R.id.buttonmedranking);
        medicamentsranking.setOnClickListener(new View.OnClickListener() {//if click on the button Medikament
            @Override
            public void onClick(View v) {
                Toast.makeText(ArztMain.this,getResources().getString(R.string.informationbestmedicine),Toast.LENGTH_LONG).show();
                Intent medranking=new Intent(ArztMain.this,Medikament.class);//If click on the button, then redirect to Medikament activity
                startActivity(medranking);
            }
        });


        //Button patients
        patienten=(Button)findViewById(R.id.buttonpatienten);
        patienten.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        Intent intent=new Intent(ArztMain.this,Patienten.class);//If click on the button, then redirect to Patienten
                        startActivity(intent);
                        Toast.makeText(ArztMain.this,getResources().getString(R.string.informationpatients),Toast.LENGTH_LONG).show();
                    }});

        //Button history
        historypatienten=(Button)findViewById(R.id.buttonhistorypatient);
        historypatienten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(ArztMain.this,MedicalHistory.class);//If click on the button, then redirect to MedicalHistory
                Toast.makeText(ArztMain.this,getResources().getString(R.string.informationpatients),Toast.LENGTH_LONG).show();
                startActivity(intent1);
            }
        });

    }


}





