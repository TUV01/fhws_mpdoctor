package com.tuv001.mpdoctor;
/**
 * Created by Magommed
 */
import android.annotation.SuppressLint;
import android.os.StrictMode;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class ConnectionDB {

    Connection con;
    String un, pass, db, ip;
    String z;


    ConnectionDB()
    {
        ip = "10.0.2.2:3306"; //Localhost
        //ip="10.31.255.254"; //Smartphone with usb-cable.
        // ip="193.174.83.97:3306";

        db = "/medtest";
        un = "root";
        pass = "";

    }

    Connection connectDB(){
        try {
            con = connectionclass(un, pass, db, ip);
        } catch (Exception ex) {
            z = ex.getMessage();
        }
        return con;
    }

    @SuppressLint("NewApi")
    Connection connectionclass(String user, String password, String database, String server) throws SQLException, ClassNotFoundException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection connection;
        String ConnectionURL;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        ConnectionURL = "jdbc:mysql://" + server + database;
        connection = DriverManager.getConnection(ConnectionURL, un, pass);

        return connection;
    }
}
