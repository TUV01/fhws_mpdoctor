package com.tuv001.mpdoctor;
/**
 * Created by JTJ-PC on 01.05.2017.
 */
//GUI (JTJ)
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.Locale;
//DB (Magommed)
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public class ArztLogin extends AppCompatActivity {
    private Button Abutton;//Login
    private EditText username;
    private EditText password;
    private String enteredusernameArzt;
    private String enteredpasswordArzt;
    private Button buttonchangelanguage;//To change the language
    private Locale locale;
    private Configuration config = new Configuration();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arzt_login);

        Abutton = (Button) findViewById(R.id.buttonALogin);
        username = (EditText) findViewById(R.id.etarztname);
        password = (EditText) findViewById(R.id.etarztpassword);
        buttonchangelanguage = (Button) findViewById(R.id.buttonchangelanguage);//buttonchangelanguage correspond with ID buttonchangelanguage from activity_arzt_login.xml

        //Button changelanguage

        buttonchangelanguage.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        showDialog();
                    }});

        //Button Login
        Abutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                enteredpasswordArzt = password.getText().toString();
                enteredusernameArzt = username.getText().toString();

                if (enteredusernameArzt.equals("") || enteredpasswordArzt.equals("")) {
                    Toast.makeText(ArztLogin.this, getString(R.string.messagefilllogin), Toast.LENGTH_LONG).show();
                    //Error msg if username or password is equals " "
                    return;
                }

                //Redirecting to ArtzMain when login is succesful
                //else created by Magommed + Toast and Intent by JTJ
                else {
                    boolean isSuccess;
                    Connection con;
                    String z = "";

                    try {
                        ConnectionDB cDB = new ConnectionDB();
                        con = cDB.connectDB(); // Connect to database
                        if (con == null) {
                            z = "Check Your Internet Access!";
                        } else {
                            String query = "select * from Doctor where drUsername = '" + enteredusernameArzt.toString() + "' and drPassword = '" + enteredpasswordArzt.toString() + "'  ";
                            Statement stmt = con.createStatement();
                            ResultSet rs = stmt.executeQuery(query);
                            if (rs.next()) {
                                z = "Login successful";
                                isSuccess = true;

                                //GUI(JTJ) from *
                                String language = Locale.getDefault().getLanguage();
                                Toast.makeText(ArztLogin.this, getString(R.string.welcomedoctor) + " " + enteredusernameArzt, Toast.LENGTH_LONG).show();
                                Intent toArztmain = new Intent(ArztLogin.this, ArztMain.class);
                                startActivity(toArztmain);
                                //to *

                                con.close();
                            } else {
                                z = "Invalid Credentials!";
                                isSuccess = false;
                            }
                        }

                    } catch (Exception ex) {

                        z = ex.getMessage();
                    }

                }

            }
        });
    }

    ////Method showDialog() created by JTJ
    /**
     * Displays a dialog box to choose the new application language
     * When you click on one of the languages, the language of the application is changed
     * And reload the activity to see the changes
     **/
    private void showDialog(){
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle(getResources().getString(R.string.changelanguage));
        // get the array languages of string.xml
        String[] types = getResources().getStringArray(R.array.languages);
        b.setItems(types, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                switch(which){
                    case 0:
                        locale = new Locale("en"); //English (default)
                        config.locale =locale;
                        break;
                    case 1:
                        locale = new Locale("es"); //Spanish
                        config.locale =locale;
                        break;
                    case 2:
                        locale = new Locale("de"); //German
                        config.locale =locale;
                        break;
                }
                getResources().updateConfiguration(config, null);
                Intent refresh = new Intent(ArztLogin.this, ArztLogin.class);
                startActivity(refresh);
                finish();
            }

        });

        b.show();
    }

}

